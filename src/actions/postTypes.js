export const Types = {
    GET_ALL: 'GET_ALL',
    GET_BY_ID: 'GET_BY_ID',
    NEW: 'NEW',
    EDIT: 'EDIT',
    DEL: 'DEL',
}