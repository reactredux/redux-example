import { Types } from './postTypes'

export const fetchPosts = () => dispatch => {
    fetch('https://jsonplaceholder.typicode.com/posts').then(res => res.json()).then(data =>
        dispatch({
            type: Types.GET_ALL,
            payload: data
        })
    )
}

export const fetchPostByID = (id) => dispatch => {
    fetch('https://jsonplaceholder.typicode.com/posts/' + id).then(res => res.json()).then(data =>
        dispatch({
            type: Types.GET_BY_ID,
            payload: data
        })
    )
}


export const editarPost = (editPost) => dispatch => {
    fetch('https://jsonplaceholder.typicode.com/posts/' + editPost.id, {
        method: 'PUT',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(editPost)
    }).then(res => res.json()).then(data =>
        dispatch({
            type: Types.EDIT,
            payload: data
        })
    )
}

export const createPost = (newPost) => dispatch => {
    fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(newPost)
    }).then(res => res.json()).then(data =>
        dispatch({
            type: Types.NEW,
            payload: data
        })
    )
}

export const deletePost = (id) => dispatch => {
    fetch('https://jsonplaceholder.typicode.com/posts/' + id, {
        method: 'DELETE',
        headers: {
            'content-type': 'application/json'
        }
    }).then(res => res.json()).then(() => {
        return (
            dispatch({
                type: Types.DEL,
                payload: id
            })
        )
    }
    )
}