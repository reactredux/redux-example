import React, { Component } from 'react'
import { connect } from 'react-redux'

import { bindActionCreators } from 'redux'
import * as postActions from '../actions/postActions'
import uuid from 'uuid'

class PostForm extends Component {

    constructor(props) {
        super(props)

        this.state = {
            id: 0,
            title: '',
            body: ''
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.item) {
            this.setState({
                id: nextProps.item.id,
                title: nextProps.item.title,
                body: nextProps.item.body
            })
        }
    }

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    onSubmit = (e) => {
        e.preventDefault()

        const newPost = {
            id: (this.state.id === 0) ? uuid() : this.state.id,
            title: this.state.title,
            body: this.state.body
        }

        if (this.state.id === 0) {
            this.props.createPost(newPost)
        } else {
            this.props.editarPost(newPost)
        }

        this.setState({
            id: 0,
            title: '',
            body: ''
        })
    }

    render() {
        return (
            <div>
                <h1>Add Post</h1>

                <form onSubmit={this.onSubmit}>
                    <div>
                        <label>Title</label><br />
                        <input type='text' name='title' value={this.state.title} onChange={this.onChange} />
                    </div>

                    <br />

                    <div>
                        <label>Body</label><br />
                        <textarea name='body' value={this.state.body} onChange={this.onChange} />
                    </div>

                    <br />

                    <div>
                        <button type='submit'>Salvar</button>
                    </div>
                </form>
            </div>
        )
    }
}

const mapStateToProps = state => ({ item: state.postsList.item })
const mapDispatchToProps = dispatch => bindActionCreators(postActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(PostForm)