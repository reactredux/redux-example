import React, { Component } from 'react'
import { connect } from 'react-redux'

import { bindActionCreators } from 'redux'
import * as postActions from '../actions/postActions'

class Posts extends Component {

    componentDidMount() {
        this.props.fetchPosts();
    }

    remover = (id) => {
        this.props.deletePost(id);
    }

    editar = (id) => {
        this.props.fetchPostByID(id);
    }

    render() {
        const { posts } = this.props

        return (
            <div>
                <h1>Posts</h1>

                {posts.map((pt, i) => (
                    <div key={i}>
                        <button onClick={() => this.remover(pt.id)}>Remover</button>
                        <button onClick={() => this.editar(pt.id)}>Editar</button>
                        - <b>{pt.id}</b> - <b>{pt.title}</b>
                        <p>{pt.body}</p>
                    </div>
                ))}
            </div>
        )
    }
}

const mapStateToProps = state => ({ posts: state.postsList.items })
const mapDispatchToProps = dispatch => bindActionCreators(postActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Posts)