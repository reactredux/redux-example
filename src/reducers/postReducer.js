import { Types } from '../actions/postTypes'

const initialState = {
    items: [],
    item: {},
    id: 0
}

export default function (state = initialState, action) {
    switch (action.type) {
        case Types.GET_ALL:
            return {
                ...state,
                items: action.payload
            }
        case Types.GET_BY_ID:
            return {
                ...state,
                item: action.payload
            }
        case Types.NEW:
            return {
                ...state,
                items: [action.payload, ...state.items]
            }
        case Types.EDIT:
            return {
                ...state,
                items: state.items.map(item => item.id === action.payload.id ? { ...action.payload, ...state.items } : item)
            }
        case Types.DEL:
            return {
                ...state,
                items: state.items.filter(item => item.id !== action.payload)
            }
        default:
            return state;
    }
}